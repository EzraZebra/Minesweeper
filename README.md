# Minesweeper
Old 2009 school project built with Qt4, ported to Qt5 in 2013.

No code changes have been made since then, except as far as was necessary to port to Qt5.  
Also included is the early CMD test version, which is in Dutch.

[Download](https://gitlab.com/EzraZebra/Minesweeper/raw/master/release/Minesweeper-win-x64.zip) (Windows x64)

![Minesweeper](https://gitlab.com/EzraZebra/Minesweeper/raw/master/release/minesweeper.jpg)
![Minesweeper Game Won](https://gitlab.com/EzraZebra/Minesweeper/raw/master/release/youwon.jpg)

![Minesweeper Game Won](https://gitlab.com/EzraZebra/Minesweeper/raw/master/release/test.jpg)